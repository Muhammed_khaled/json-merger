import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File

class Merger {

    fun main() {
        val file1 = "path1/hisnmuslim.json"
        val file2 = "path2/husn_en.json"
        val outputFile = "merged.json"

        val gson = Gson()

        // Load data from both files
        val data1 = gson.fromJson(File(file1).readText(), List::class.java) as List<Map<String, Any>>
        val data2 = gson.fromJson(File(file2).readText(), List::class.java) as List<Map<String, Any>>

        // Merge data based on common audio filename
        val mergedData = mutableListOf<Map<String, Any>>()
        for (obj1 in data1) {
            val audio1 = obj1["audio"] as String
            val matchingObj2 = data2.find { (it["AUDIO_URL"] as String).endsWith(audio1.takeLast(7)) }

            if (matchingObj2 != null) {
                println("Hello inside matchingObj2 ${matchingObj2["AUDIO_URL"]}")
                val mergedObj = obj1.toMutableMap()
                mergedObj["category_eng"] = matchingObj2["TITLE"] as Any

                // Merge text data for each child object
                val textList1 = obj1["array"] as ArrayList<Map<String, Any>>
                val textList2 = matchingObj2["TEXT"] as ArrayList<Map<String, Any>>
                textList1.forEachIndexed { index, textObj1 ->
//                    val matchingTextObj2 = textList2.find { it["ID"] == textObj1["id"] }
                    val childAudio = textObj1["audio"] as String
                    val matchingTextObj2 = textList2.find { (it["AUDIO"] as String).endsWith(childAudio.takeLast(7)) }
//                    val matchingTextObj2 = textList2.find { it["ID"].toString() == textObj1["filename"].toString()  }

                    println("Hello inside matchingTextObj2 ${matchingTextObj2}")

                    if (matchingTextObj2 != null) {
                        println("Hello inside textList1$matchingTextObj2")

                        // Cast to mutable map if necessary
                        val mutableTextObj1 = textObj1.toMutableMap()

                        // Handle optional values with safe access
                        mutableTextObj1["translated_text"] = matchingTextObj2["TRANSLATED_TEXT"]?.let { it } ?: ""
                        mutableTextObj1["language_ar_translated_text"] = matchingTextObj2["LANGUAGE_ARABIC_TRANSLATED_TEXT"]?.let { it } ?: ""

                        println("Hello before update ${textList1[index]} \n and the object will update ${mutableTextObj1}")
                        // Update textList1 with the modified object
                        textList1[index] = mutableTextObj1

                    }
                }

                mergedData.add(mergedObj)
            }
        }

        // Write merged data to output file
        val outputString = gson.toJson(mergedData)
        File(outputFile).writeText(outputString)
    }

}